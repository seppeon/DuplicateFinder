#include <filesystem>
#include <fstream>
#include <ios>
#include <iostream>
#include <ranges>
#include <span>
#include <streambuf>
#include <string>
#include <thread>
#include <unordered_map>
#include <unordered_set>
#include <vector>

namespace
{
	auto get_args(int argc, char ** argv) -> std::vector<std::filesystem::path>
	{
		std::vector<std::filesystem::path> output;
		for (int i = 1; i < argc; ++i) output.push_back({argv[i]});
		return output;
	}

	auto open_file(std::filesystem::path p) -> std::vector<char>
	{
		std::ifstream file(p, std::ios_base::in | std::ios_base::binary);
		return { std::istreambuf_iterator<char>(file), std::istreambuf_iterator<char>() };
	}

	auto hash_file(std::filesystem::path p) -> std::size_t
	{
		auto const file_contents = open_file(p);
		return std::hash<std::string_view>{}(std::string_view{file_contents.data(), file_contents.size()});
	}

	struct iterable
	{
		std::filesystem::recursive_directory_iterator b;
		std::filesystem::recursive_directory_iterator e = {};

		iterable(std::filesystem::path p)
			: b{ p }
		{
		}

		auto begin() const noexcept { return b; }
		auto end() const noexcept { return e; }
	};

	struct file_counts
	{
		std::size_t file_tally = 0;
		std::unordered_map<std::size_t, std::unordered_set<std::filesystem::path>> files;

		void merge(file_counts && counts)
		{
			file_tally += counts.file_tally;
			files.merge(std::move(counts.files));
		}

		void emplace(std::filesystem::path p)
		{
			files[hash_file(p)].insert(p);
			++file_tally;
		}
	};

	struct file_search_thread
	{
		file_search_thread(std::filesystem::path dir, file_counts & file_counts)
			: m_dir{ dir } , m_output_file_counts{ &file_counts }, m_worker([&]{(*this)();}){}
		~file_search_thread()
		{
			if (m_worker.joinable()) m_worker.join();
			m_output_file_counts->merge(std::move(m_file_counts));
		}
		void operator()() noexcept
		{
			if (not std::filesystem::is_directory(m_dir))
			{
				std::cout << m_dir << " is not a directory, ignoring.\n";
				return;
			}
			auto const recurse_dirs = iterable{ m_dir };
			for (auto const recurse_dir : recurse_dirs)
			{
				if (std::filesystem::is_regular_file(recurse_dir))
				{
					m_file_counts.emplace(recurse_dir);
				}
			}
		}
	private:
		std::filesystem::path m_dir;
		file_counts m_file_counts;
		file_counts * m_output_file_counts;
		std::thread m_worker;
	};

	file_counts perform_work(std::vector<std::filesystem::path> const & dirs)
	{
		file_counts counts;
		auto const dirs_range = dirs | std::views::transform([&](std::filesystem::path dir){
			return new file_search_thread{dir, counts};
		});
		[[maybe_unused]] std::vector<std::unique_ptr<file_search_thread>> worker_theads{ dirs_range.begin(), dirs_range.end() };
		return counts;
	}
}

int main(int argc, char ** argv)
{
	auto const dirs = get_args(argc, argv);

	// Find the duplicates
	auto file_counts = perform_work(dirs);

	// Print the results
	auto const unique_file_count = file_counts.files.size();
	auto const total_files = file_counts.file_tally;
	std::cout << "unique_file_count: " << unique_file_count << "\n";
	std::cout << "total_files: " << total_files << "\n";
	if (unique_file_count != total_files)
	{
		std::cout << "Duplicate file list:\n";
		for (auto const & [hash, dups] : file_counts.files)
		{
			if (dups.size() == 1) continue;
			std::cout << " - hash: " << hash << "\n";
			for (auto const & dup : dups)
			{
				std::cout << "\t - " << dup << "\n";
			}
			std::cout << "\n";
		}
	}
}