Search a list of provided directories for possible duplicate files, returns a list of these files. This application is build with cmake, and requires clang, gcc or msvc to be installed.

If you wanted to seach these three folders:
 - c:\temp\docs
 - c:\bin
 - d:\cats

Then you could execute the application as follows:
```
DuplicateFinder.exe c:\temp\docs c:\bin d:\cats
```
